package pl.niezawodnykod.aeron;

import lombok.Value;

@Value
public class Message {
    String content;
}
