package pl.niezawodnykod.aeron.communication.jms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.core.JmsTemplate;
import pl.niezawodnykod.aeron.traffic.TrafficReceiver;

@Profile("jms-communication")
@Configuration
public class JmsCommunicationConfiguration {

    @Profile("producer")
    @Bean
    public JmsBasedProducer producer(JmsTemplate jmsTemplate) {
        return new JmsBasedProducer(jmsTemplate);
    }

    @Profile("consumer")
    @Bean
    public JmsBasedConsumer consumer(TrafficReceiver trafficReceiver) {
        return new JmsBasedConsumer(trafficReceiver);
    }
}
