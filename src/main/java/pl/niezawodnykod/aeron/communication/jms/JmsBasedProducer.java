package pl.niezawodnykod.aeron.communication.jms;

import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import pl.niezawodnykod.aeron.Producer;

@RequiredArgsConstructor
public class JmsBasedProducer implements Producer {

    private final JmsTemplate jmsTemplate;

    public void publish(String message) {
        jmsTemplate.convertAndSend(message);
    }
}
