package pl.niezawodnykod.aeron.communication.jms;

import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import pl.niezawodnykod.aeron.traffic.TrafficReceiver;

@RequiredArgsConstructor
public class JmsBasedConsumer {

    private final TrafficReceiver receiver;

    @JmsListener(destination = "${spring.jms.template.default-destination}")
    public void receive(String message) {
        receiver.process(message);
    }
}
