package pl.niezawodnykod.aeron.communication.aeron;

import io.aeron.Publication;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class AeronResultStateLogger {
    void describeIfFailedPublication(long resultCode) {
        if (resultCode > 0) {
            // valid publication
        } else if (resultCode == Publication.BACK_PRESSURED) {
            log.info("Offer failed due to back pressure");
        } else if (resultCode == Publication.NOT_CONNECTED) {
            log.info("Offer failed because publisher is not connected to a subscriber");
        } else if (resultCode == Publication.ADMIN_ACTION) {
            log.info("Offer failed because of an administration action in the system");
        } else if (resultCode == Publication.CLOSED) {
            log.info("Offer failed because publication is closed");
        } else if (resultCode == Publication.MAX_POSITION_EXCEEDED) {
            log.info("Offer failed due to publication reaching its max position");
        } else {
            log.info("Offer failed due to unknown reason: " + resultCode);
        }
    }
}
