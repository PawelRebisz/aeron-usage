package pl.niezawodnykod.aeron.communication.aeron;

import io.aeron.Aeron;
import io.aeron.Publication;
import lombok.extern.slf4j.Slf4j;
import org.agrona.BufferUtil;
import org.agrona.concurrent.UnsafeBuffer;
import pl.niezawodnykod.aeron.Producer;

import java.io.Closeable;

@Slf4j
public class AeronBasedProducer implements Producer, Closeable {

    private final AeronResultStateLogger aeronResultStateLogger;
    private final Aeron aeron;
    private final Publication publication;

    public AeronBasedProducer(AeronResultStateLogger aeronResultStateLogger, int streamId, String channel) {
        this.aeronResultStateLogger = aeronResultStateLogger;

        Aeron.Context context = new Aeron.Context();
        aeron = Aeron.connect(context);
        publication = aeron.addPublication(channel, streamId);
        log.info("Created aeron producer at channel {} with stream {}.", channel, streamId);
    }

    @Override
    public void close() {
        publication.close();
        aeron.close();
    }

    @Override
    public void publish(String jsonAsString) {
        UnsafeBuffer buffer = new UnsafeBuffer(BufferUtil.allocateDirectAligned(256, 64));
        byte[] bytes = jsonAsString.getBytes();
        buffer.putBytes(0, bytes);
        long result = publication.offer(buffer, 0, bytes.length);

        aeronResultStateLogger.describeIfFailedPublication(result);
        if (!publication.isConnected()) {
            log.info("No active subscribers detected");
        }
    }
}
