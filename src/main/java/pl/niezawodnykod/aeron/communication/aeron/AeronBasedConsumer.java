package pl.niezawodnykod.aeron.communication.aeron;

import io.aeron.Aeron;
import io.aeron.Subscription;
import io.aeron.logbuffer.FragmentHandler;
import lombok.extern.slf4j.Slf4j;
import org.agrona.concurrent.BackoffIdleStrategy;
import org.agrona.concurrent.IdleStrategy;
import pl.niezawodnykod.aeron.traffic.TrafficReceiver;

import java.io.Closeable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.concurrent.TimeUnit.MICROSECONDS;

@Slf4j
public class AeronBasedConsumer implements Closeable {

    private final Aeron aeron;
    private final Subscription subscription;
    private final TrafficReceiver receiver;

    private final AtomicBoolean running = new AtomicBoolean(true);

    public AeronBasedConsumer(ExecutorService executorService, TrafficReceiver aReceiver,
                              int streamId, String channel) {
        this.receiver = aReceiver;

        Aeron.Context context = new Aeron.Context();
        aeron = Aeron.connect(context);
        subscription = aeron.addSubscription(channel, streamId);
        log.info("Created aeron consumer at channel {} with stream {}.", channel, streamId);

        executorService.execute(consumeFragmentsWhileRunning(subscription));
    }

    private Runnable consumeFragmentsWhileRunning(Subscription subscription) {
        int fragmentLimitCount = 10;
        FragmentHandler fragmentHandler = (buffer, offset, length, header) -> {
            final byte[] data = new byte[length];
            buffer.getBytes(offset, data);
            String message = new String(data);
            receiver.process(message);
        };
        final IdleStrategy idleStrategy = new BackoffIdleStrategy(100, 10,
                MICROSECONDS.toNanos(1), MICROSECONDS.toNanos(100));

        return () -> {
            while (running.get()) {
                final int fragmentsRead = subscription.poll(fragmentHandler, fragmentLimitCount);
                idleStrategy.idle(fragmentsRead);
            }
        };
    }

    @Override
    public void close() {
        running.set(false);

        subscription.close();
        aeron.close();
    }
}
