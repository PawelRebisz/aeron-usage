package pl.niezawodnykod.aeron.communication.aeron;

import io.aeron.driver.MediaDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import pl.niezawodnykod.aeron.traffic.TrafficReceiver;

import java.util.concurrent.ExecutorService;

@Profile("aeron-communication")
@Configuration
public class AeronCommunicationConfiguration {

    private static final int STREAM_ID = 1001;
    private static final String CHANNEL = "aeron:udp?endpoint=localhost:20121";

    @Profile("producer")
    @Bean(destroyMethod = "close")
    public MediaDriver mediaDriver() {
        return MediaDriver.launchEmbedded();
    }

    @Profile("producer")
    @Bean(destroyMethod = "close")
    public AeronBasedProducer producer() {
        return new AeronBasedProducer(new AeronResultStateLogger(), STREAM_ID, CHANNEL);
    }

    @Profile("consumer")
    @Bean(destroyMethod = "close")
    public AeronBasedConsumer consumer(ExecutorService executorService, TrafficReceiver receiver) {
        return new AeronBasedConsumer(executorService, receiver, STREAM_ID, CHANNEL);
    }
}
