package pl.niezawodnykod.aeron.communication.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;
import pl.niezawodnykod.aeron.Producer;

@Slf4j
public class RestBasedProducer implements Producer {

    private final RestTemplate template;

    public RestBasedProducer(RestTemplate template) {
        this.template = template;
    }

    @Override
    public void publish(String jsonAsString) {
        template.postForObject("http://localhost:4000/api/v1/event", jsonAsString, String.class);
    }
}
