package pl.niezawodnykod.aeron.communication.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;
import pl.niezawodnykod.aeron.traffic.TrafficReceiver;

@Profile("rest-communication")
@Configuration
public class RestCommunicationConfiguration {

    @Profile("consumer")
    @Bean
    public RestBasedConsumer consumer(TrafficReceiver trafficReceiver) {
        return new RestBasedConsumer(trafficReceiver);
    }

    @Profile("producer")
    @Bean
    public RestBasedProducer producer() {
        return new RestBasedProducer(new RestTemplate());
    }
}
