package pl.niezawodnykod.aeron.communication.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.niezawodnykod.aeron.traffic.TrafficReceiver;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class RestBasedConsumer {

    private final TrafficReceiver receiver;

    @PostMapping("/event")
    public String receive(@RequestBody String jsonAsString) {
        receiver.process(jsonAsString);
        return "";
    }
}
