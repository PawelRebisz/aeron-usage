package pl.niezawodnykod.aeron.traffic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RequiredArgsConstructor
public class TrafficReceiver {

    private final AtomicInteger receivedMessageCount = new AtomicInteger(0);

    public void process(String jsonAsString) {
        receivedMessageCount.incrementAndGet();
        log.debug("Received message with content: {}.", jsonAsString);
    }

    public int getReceivedMessageCount() {
        return receivedMessageCount.get();
    }
}
