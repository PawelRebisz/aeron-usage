package pl.niezawodnykod.aeron.traffic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import pl.niezawodnykod.aeron.Message;
import pl.niezawodnykod.aeron.Producer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class TrafficGenerator {

    private final AtomicInteger publishedMessageCount = new AtomicInteger(0);
    private final Producer producer;
    private final ObjectMapper mapper;

    public TrafficGenerator(ExecutorService executorService, Producer aProducer, ObjectMapper mapper) {
        this.producer = aProducer;
        this.mapper = mapper;

        executorService.execute(() -> {
            for (long count = 0; ; count++) {
                Message message = new Message("message-" + count);
                String jsonAsString = objectAsString(message);
                producer.publish(jsonAsString);
                publishedMessageCount.incrementAndGet();
                log.debug("Published message: {}", message);
            }
        });
    }

    private String objectAsString(Message message) {
        try {
            return mapper.writeValueAsString(message);
        } catch (JsonProcessingException exception) {
            throw new RuntimeException("Something went really badly.", exception);
        }
    }

    public int getPublishedMessageCount() {
        return publishedMessageCount.get();
    }
}
