package pl.niezawodnykod.aeron.traffic;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.niezawodnykod.aeron.Producer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class TrafficConfiguration {

    @Bean
    public ProducerMonitoringInfluxTicker producerMonitoringInfluxTicker(
            ExecutorService executorService,
            TrafficGenerator trafficGenerator,
            TrafficReceiver trafficReceiver
    ) {
        return new ProducerMonitoringInfluxTicker(executorService, trafficGenerator, trafficReceiver);
    }

    @Bean
    public TrafficGenerator trafficGenerator(ExecutorService executorService,
                                             Producer producer,
                                             ObjectMapper objectMapper) {
        return new TrafficGenerator(executorService, producer, objectMapper);
    }

    @Bean
    public TrafficReceiver trafficReceiver() {
        return new TrafficReceiver();
    }

    @Bean(destroyMethod = "shutdownNow")
    public ExecutorService executorService() {
        return Executors.newFixedThreadPool(5);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
