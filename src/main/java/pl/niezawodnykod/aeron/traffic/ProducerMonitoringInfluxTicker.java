package pl.niezawodnykod.aeron.traffic;

import lombok.extern.slf4j.Slf4j;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ProducerMonitoringInfluxTicker {

    private final TrafficGenerator trafficGenerator;
    private final TrafficReceiver trafficReceiver;

    public ProducerMonitoringInfluxTicker(ExecutorService executorService,
                                          TrafficGenerator trafficGenerator,
                                          TrafficReceiver trafficReceiver) {
        this.trafficGenerator = trafficGenerator;
        this.trafficReceiver = trafficReceiver;
        executorService.execute(this::publishPublicationAndConsumptionStatistics);
    }

    private void publishPublicationAndConsumptionStatistics() {
        final String serverURL = "http://127.0.0.1:8086", username = "user", password = "password";
        final InfluxDB influxDB = InfluxDBFactory.connect(serverURL, username, password);
        influxDB.setDatabase("monitoring");

        long startTimestamp = System.currentTimeMillis();
        try {
            Thread.sleep(1000); // wait to avoid a initial blip on the chart
            while (true) {
                Point point = pointOfMeasurement(startTimestamp);
                influxDB.write(point);

                Thread.sleep(1_000L);
            }
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception);
        }
    }

    private Point pointOfMeasurement(long startTimestamp) {
        long numberOfMessagesSend = trafficGenerator.getPublishedMessageCount();
        long numberOfMessagesConsumed = trafficReceiver.getReceivedMessageCount();
        long msgSend = throughputFrom(startTimestamp, numberOfMessagesSend);
        long msgConsumed = throughputFrom(startTimestamp, numberOfMessagesConsumed);
        Point.Builder performance = Point.measurement("performance")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("msgSend", msgSend)
                .addField("msgConsumed", msgConsumed);
        Point point = performance.build();
        log.info("Publishing point: {}", point);
        return point;
    }

    private long throughputFrom(long timestamp, long messages) {
        long millisecondsDifference = System.currentTimeMillis() - timestamp;
        return messages * 1000 / millisecondsDifference;
    }
}
