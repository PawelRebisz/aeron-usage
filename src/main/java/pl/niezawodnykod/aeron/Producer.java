package pl.niezawodnykod.aeron;

public interface Producer {
    void publish(String jsonAsString);
}
