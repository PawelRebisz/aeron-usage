package pl.niezawodnykod.aeron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.util.StringUtils;
import pl.niezawodnykod.aeron.communication.aeron.AeronCommunicationConfiguration;
import pl.niezawodnykod.aeron.communication.jms.JmsCommunicationConfiguration;
import pl.niezawodnykod.aeron.communication.rest.RestCommunicationConfiguration;
import pl.niezawodnykod.aeron.traffic.TrafficConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@EnableAutoConfiguration
@ImportAutoConfiguration({
        TrafficConfiguration.class,
        RestCommunicationConfiguration.class,
        AeronCommunicationConfiguration.class,
        JmsCommunicationConfiguration.class
})
public class AeronUsageApplication {

    public static void main(String[] args) {
        if (StringUtils.isEmpty(System.getenv("aeron.dir"))) {
            Path path = getTemporaryFolder("aeron-directory").toAbsolutePath();
            System.setProperty("aeron.dir", path.toString());
        }
        SpringApplication application = new SpringApplication(AeronUsageApplication.class);
        if (application.getAdditionalProfiles().isEmpty()) {
            application.setAdditionalProfiles("aeron-communication", "consumer", "producer");
        }
        application.run(args);
    }

    private static Path getTemporaryFolder(String prefix) {
        try {
            return Files.createTempDirectory(prefix);
        } catch (IOException exception) {
            throw new RuntimeException("Something went really south.", exception);
        }
    }
}

