FROM maven:3.6.3-openjdk-15 AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn clean package

FROM adoptopenjdk/openjdk15
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/aeron-usage-1.0.0-SNAPSHOT-jar-with-dependencies.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080

ENTRYPOINT ["java", "-jar", "aeron-usage-1.0.0-SNAPSHOT-jar-with-dependencies.jar"]
