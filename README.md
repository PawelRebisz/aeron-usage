# Aeron Usage

For more look at [NiezawodnyKod.pl](https://niezawodnykod.pl)

### Usage
Before running [AeronUsageApplication](src/main/java/pl/niezawodnykod/aeron/AeronUsageApplication.java)
run:
> docker-compose up

To have `grafana` dashboard available to monitor the amount of messages flowing through the example.